from django.db import models
from smart_selects.db_fields import ChainedForeignKey

from client.models import Client


class City(models.Model):
    name = models.CharField(max_length=255, verbose_name='Город')

    class Meta:
        verbose_name = 'Город'
        verbose_name_plural = "Городы"
        ordering = ['id']

    def __str__(self):
        return self.name


class Restaurant(models.Model):
    class Meta:
        verbose_name = 'Ресторан'
        verbose_name_plural = 'Рестораны'
        ordering = ['id']

    city = models.ForeignKey(City, models.CASCADE)
    name = models.CharField(max_length=100, verbose_name='Ресторан')
    address = models.CharField(max_length=255)
    phone = models.IntegerField()

    def __str__(self):
        return self.name


class Menu(models.Model):
    restaurant = models.ForeignKey(Restaurant, models.SET_NULL, null=True, blank=True)
    name = models.CharField(max_length=255)

    class Meta:
        verbose_name = 'Меню'
        verbose_name_plural = "Меню"
        ordering = ['id']

    def __str__(self):
        return self.name


class Food(models.Model):
    menu = models.ForeignKey(Restaurant, models.SET_NULL, null=True, blank=True)
    sub_menu = ChainedForeignKey(
        Menu,
        chained_field="menu",
        chained_model_field="restaurant",
        show_all=False,
        auto_choose=True,
        sort=True,
    )
    name = models.CharField(max_length=255)
    CUR = [
        ('COM', 'сом'),
        ('USD', 'доллар'),
        ('RUB', 'руб'),
    ]
    order_type = models.CharField(max_length=40, choices=CUR, null=True, blank=True)
    ingredients = models.TextField()
    photo = models.ImageField(upload_to='product/', null=True, blank=True)
    amount = models.PositiveIntegerField(verbose_name='Кол-во', default=1)
    price = models.IntegerField(verbose_name='Цена', null=True, blank=True)
    pub_date = models.DateField(verbose_name='Дата публикации', auto_now_add=True, blank=True, null=True)

    class Meta:
        verbose_name = 'Еда'
        verbose_name_plural = "Еды"
        ordering = ['id']

    def __str__(self):
        return '{name} {s_menu}'.format(name=self.name, s_menu=self.sub_menu)


class Order(models.Model):
    PLATA = [
        ('cash', 'Наличка'),
        ('card', 'Карта'),
    ]
    customer = models.ForeignKey(to=Client, on_delete=models.CASCADE, verbose_name='Клиент')
    plata = models.CharField(verbose_name='Платеж', choices=PLATA, default='CARD', max_length=4)
    address = models.CharField(verbose_name='Адрес', max_length=100)

    def __str__(self):
        return f'{self.customer}'

    class Meta:
        verbose_name = 'Заказчик'
        verbose_name_plural = 'Заказчики'


class ItemOrders(models.Model):
    product = models.ForeignKey(Food, models.SET_NULL, null=True, verbose_name='Еда')
    kol = models.IntegerField(verbose_name='Кол-во', default=1)
    order = models.ForeignKey(Order, models.SET_NULL, null=True, blank=True, verbose_name='Заказчик')

    def __str__(self):
        return '{}'.format(self.order)

    class Meta:
        verbose_name_plural = 'Заказы'
        verbose_name = 'Заказ'
