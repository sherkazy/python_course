from django.urls import path, include
from user.views import *

urlpatterns = [
    path('', include('django.contrib.auth.urls')),
    path('register/', register, name='register'),
    path('login_shopx/', login_own, name='login_own'),
    path('comment/', comment, name='comment'),
    path('logout/', logout_own, name='logout_own'),
]