from django.contrib import admin
from django.db.models import QuerySet

from client.models import Client


class ClientAdmin(admin.ModelAdmin):
    list_display = ['name', 'surname', 'phone_number', 'age', 'address']
    list_display_links = ['name', 'surname']
    list_per_page = 5
    search_fields = ['name', 'surname', 'address']
    list_editable = ['age', 'address']
    ordering = ['name', ]
    actions = ['change_name', 'change_phone_number']

    @admin.action(description='Изменить возраст в 20')
    def change_name(self, request, qs: QuerySet):
        qs.update(age=20)

    @admin.action(description='Изменить номер телефона на 996778678998')
    def change_phone_number(self, request, qs: QuerySet):
        qs.update(phone_number=996778678998)


admin.site.register(Client, ClientAdmin)