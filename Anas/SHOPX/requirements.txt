Django==3.2
Pillow==9.1.1
django-smart-selects==1.5.9
asgiref==3.5.2
sqlparse==0.4.2
django-admin-interface