from django.db import models
from django.urls import reverse
from smart_selects.db_fields import ChainedForeignKey

class Establishment(models.Model):
    title = models.CharField(max_length=255)
    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Заведение'
        verbose_name_plural = 'Заведения'



class Category(models.Model):
    establishment = models.ForeignKey(Establishment, models.CASCADE)
    title = models.CharField(max_length=255)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'котегория'
        verbose_name_plural = 'котегории'

class Product(models.Model):
    CHOICES = (
        ('available','в наличии'),
        ('not available','нет в наличии')
    )
    title = models.CharField(max_length=255,name='тема')
    establishment = models.ForeignKey(Establishment, models.SET_NULL, null=True, blank=True,name='Заведение')
    category = ChainedForeignKey(
        Category,
        chained_field="establishment",
        chained_model_field='establishment', null=True, blank=True,name='Котегория')
    #sub_category = models.ForeignKey(Category, models.SET_NULL, null=True, blank=True)
    article_number = models.IntegerField()
    price = models.IntegerField(name='Цена')
    photo = models.ImageField(upload_to='product/',name='Фото')
    available = models.CharField(max_length=150,null=False,choices=CHOICES)
    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Продукт'
        verbose_name_plural = 'Продукты'
class Order(models.Model):
    dateTimeOfPosting = models.DateTimeField(auto_now_add=True)
    name = models.CharField(max_length=100)
    is_cash = models.BooleanField()
    total_price = models.IntegerField()
    address = models.TextField()
    '''CHOICES = (
        ('+996', 'кыргызстан +996'),
        ('+7', 'россия +7')
    )
    numbers_type = models.CharField(max_length=150,null=False,choices=CHOICES)'''
    total_item = models.IntegerField()
    class Meta:
        verbose_name = 'Заказ'
        verbose_name_plural = 'Заказы'
        ordering = ["-dateTimeOfPosting"]

class ItemOrder(models.Model):
    product = models.ForeignKey(Product, models.SET_NULL, null=True, blank=True)
    product_name = models.CharField(max_length=255)
    order = models.ForeignKey(Order, models.SET_NULL, null=True, blank=True)
    quantity = models.IntegerField()