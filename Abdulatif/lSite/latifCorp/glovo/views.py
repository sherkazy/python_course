from django.shortcuts import render
from .models import *


def index(request):
    est = Establishment.objects.all()
    return render(request, 'glovo/index.htm', {'est': est,'title': 'Glovo'})


def about(request):
    return render(request, 'glovo/about.html', {'title': 'О нас'})


def Cotegory(request,est_id):
    est = Establishment.objects.get(id=est_id)
    cat = Category.objects.filter(establishment = est_id)
    context = {
        'title': est.title,
        'cat':cat
    }
    return render(request, 'glovo/establishment.html', context)
