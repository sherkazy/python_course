from django.core.exceptions import ValidationError
from django.db import models


class Category(models.Model):
    title = models.CharField(max_length=255)
    def __str__(self):
        return self.title

class SubCategory(models.Model):
    category = models.ForeignKey(Category, models.CASCADE)
    title = models.CharField(max_length=255)
    def __str__(self):
        return self.title


class Currency(models.Model):
    title = models.CharField(max_length=255)
    def __str__(self):
        return self.title


class Product(models.Model):
    title = models.CharField(max_length=255)
    sub_category = models.ForeignKey(SubCategory, models.SET_NULL, null=True, blank=True)
    article_number = models.IntegerField()
    price = models.IntegerField()
    currency = models.ForeignKey(Currency, models.SET_NULL, null=True, blank=True)
    photo = models.ImageField(upload_to='product/')
    def __str__(self):
        return self.title


number_codes_kg = [550, 551, 552, 553, 554, 555, 556, 557, 558, 559,700, 701, 702, 703, 704, 705, 706, 707, 708, 709, 220, 221, 222, 223, 224, 225, 226, 227, 228, 229, 220, 221, 222, 223, 224, 225, 226, 227, 228, 229, 700, 701, 702, 703, 704, 705, 706, 707, 708, 709]
class Order(models.Model):
    name = models.CharField(max_length=100)
    is_delivery = models.BooleanField(default=True)
    is_cash = models.BooleanField()
    total_price = models.IntegerField()
    address = models.TextField()
    total_item = models.IntegerField()
    CHOICES = (
        ('+996', 'кыргызстан +996'),
        ('+7', 'россия +7')
    )
    numbers_type = models.CharField(max_length=150, null=False, choices=CHOICES)
    number = models.CharField(max_length=13,null=False)
    def clean(self,ch = CHOICES):
        if self.numbers_type == ch[0] and self.number[:3] not in number_codes_kg:
            raise ValidationError('мындай номер жок')
    def __str__(self):
        return self.title


class ItemOrder(models.Model):
    product = models.ForeignKey(Product, models.SET_NULL, null=True, blank=True)
    product_name = models.CharField(max_length=255)
    order = models.ForeignKey(Order, models.SET_NULL, null=True, blank=True)
    quantity = models.IntegerField()
    def __str__(self):
        return self.title
