from django.contrib import admin
from .models import *
from django.db.models import F

# Register your models here.
admin.site.site_header = "KR STORE"
admin.site.site_title = "KR Admin Portal"
admin.site.index_title = "Welcome to KR Researcher Portal"
admin.site.register(Customer)

#admin.site.register(Order)
#admin.site.register(OrderItem)
#admin.site.register(ShippingAddress)


class ProductAdmin(admin.ModelAdmin):
    list_display = ['name', 'price', 'digital', 'image']
    list_editable = ['price', 'digital']
    search_fields = ['name']
    list_filter = ['price']

    actions = ['apply_discount']

    def apply_discount(self, request, queryset):
        queryset.update(price=F('price') * float(0.9))

    apply_discount.short_description = '10%% арзандатуу'


admin.site.register(Product, ProductAdmin)


class CategoryAdmin(admin.ModelAdmin):
    list_display = ['name', 'title', 'id']


admin.site.register(Category, CategoryAdmin)


class SupCategoryAdmin(admin.ModelAdmin):
    list_display = ['name', 'title', 'id']


admin.site.register(SupCategory, SupCategoryAdmin)
