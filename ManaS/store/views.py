from django.shortcuts import render
from .models import *
from django.http import HttpResponse
import json


def store(request):
    # products = Category.objects.all().values('name', 'title', 'id')
    products = Product.objects.all()
    context = {'products': products}
    # return HttpResponse(json.dumps(result), content_type='application/json')
    return render(request, 'Store/store.html', context)


def cat(request):
    category = Category.objects.all()
    context = {'category': category}
    return render(request, 'Store/category.html', context)


def product(request):
    products1 = Product.objects.all()
    context = {'products': products1}
    return render(request, 'Store/product.html', context)


def cart(request):
    if request.user.is_authenticated:
        customer = request.user.customer
        order, created = Order.objects.get_or_create(customer=customer, complete=False)
        items = order.orderitem_set.all()
    else:
        items = []

    context = {'items', items}
    return render(request, 'Store/cart.html', context)


def menu(request):
    sp = SupCategory.objects.all()
    context = {'sp': sp}
    return render(request, 'Store/menu.html', context)


def menu1(request):
    dr = Category.objects.all()
    context = {'dr': dr}
    return render(request, 'Store/menu.html', context)


# def name(request):
#   pr = Product.objects.all()
#  context = {'pr': pr}
# return render(request, 'Store/main.html', context)
def test(request):
    qs = SupCategory.objects.all().values('id', 'categoty_to_supcategory', 'name', 'title')
    res = list(qs)
    return HttpResponse(json.dumps(res))
