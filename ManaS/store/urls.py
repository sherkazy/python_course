from django.urls import path
from . import views

urlpatterns = [
    path('', views.store, name='store'),
    path('category/', views.cat, name='category'),
    path('cart/', views.cart, name='cart'),
    path('menu/', views.menu, name='menu'),
    path('product/', views.product, name='products'),
    path('test/', views.test),


]
