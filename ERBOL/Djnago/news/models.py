from django.db import models

# Create your models here.
class History(models.Model):
    title = models.CharField(max_length=100)
    text = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)
    file = models.FileField(upload_to='img', null=True)

    def str(self):
        return self.title
