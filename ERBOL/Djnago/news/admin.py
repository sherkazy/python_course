from django.contrib import admin
from news.models import History
# Register your models here.

class NewsAdmin(admin.ModelAdmin):
    list_display = ['name', 'text', 'created_at']
    list_display_links = ['text', 'created_at']
    list_editable = ['name']
    search_fields = ['name', 'text']


admin.site.register(History, NewsAdmin)
admin.site.register(History)