from django.urls import path
from . import views

urlpatterns = [
    path('', views.main, name='main'),
    path('catalog/<int:catalog_id>/subs', views.c_subs, name='catalogs_subs'),
    path('catalog/<int:catalog_id>/<int:sub_id>/product', views.s_products, name="subs_product"),
    path('catalog/product', views.products, name='product'),
    path('card', views.form, name='form'),
    path('client', views.client, name='client'),
    path('add_product', views.add_product, name='add_product')
]