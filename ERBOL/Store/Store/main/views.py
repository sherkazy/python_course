import json

from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.views.generic import View
from catalog.models import *


def main(request):
    com = Catalog.objects.all()
    return render(request, '../templates/main.html', context={'title': 'Главная страница сайта', 'catalog': com})
    # return HttpResponse(json.dumps(res), content_type='application/json')


def catalog(request):
    return render(request, './catalog/catalog.html')


def header(request):
    return render(request, 'header.html')


def products(request):
    product = Product.objects.all
    return render(request, 'product.html', context={'product': product})


def c_subs(request, catalog_id):
    c_sub = SubCatalog.objects.filter(catalog_id=catalog_id)
    return render(request, 'catalog/sub_catalog.html', context={'sub_catalog': c_sub})


def s_products(request, catalog_id, sub_id):
    s_product = Product.objects.filter(sub_catalog_id=sub_id)
    return render(request, 'product.html', context={'product': s_product})


def sub_cat(request):
    sub = SubCatalog.objects.all()
    return render(request, 'catalog/sub_catalog.html', context={'sub': sub})


def form(request):
    form = Costumer.objects.all()
    return render(request, 'form_main.html', context={'costumer': form})


def client(request):
    name = request.POST['fname']
    surname = request.POST['lname']
    age = request.POST['age']
    gender = request.POST['gender']
    number = request.POST['number']

    model = Costumer()

    model.name = name
    model.age = age
    model.gender = gender
    model.phone_number = number

    model.save()

    return redirect('form')


def add_product(request):
    return render(request, 'add_product.html')