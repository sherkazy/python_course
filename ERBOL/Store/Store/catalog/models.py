from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from django.db import models
from smart_selects.db_fields import ChainedForeignKey

import users
from main import *


class Currency(models.Model):
    class Meta:
        verbose_name = 'Курс'
        verbose_name_plural = 'Курсы'

    title = models.CharField('Курс', max_length=255)

    def __str__(self):
        return self.title


class Catalog(models.Model):
    class Meta:
        verbose_name = 'Каталог'
        verbose_name_plural = 'Каталогы'

    name = models.CharField('Имя каталога', max_length=255)
    image = models.ImageField('Картина каталога')
    title = models.TextField('Описание', null=True, blank=True)

    @property
    def imageURL(self):
        try:
            return self.image.url
        except:
            return ''

    def __str__(self):
        return self.name

class SubCatalog(models.Model):
    class Meta:
        verbose_name = 'Под каталог'
        verbose_name_plural = 'Под каталог'

    name = models.CharField('Имя под-каталога', max_length=255)
    catalog = models.ForeignKey(Catalog, models.SET_NULL, null=True, verbose_name='Каталог')
    image = models.ImageField('Картинка под-каталога')

    def __str__(self):
        return self.name


class Product(models.Model):
    class Meta:
        verbose_name = 'Товар'
        verbose_name_plural = 'Товары'

    catalog = models.ForeignKey(verbose_name='Каталог', to=Catalog, on_delete=models.SET_NULL, null=True)

    sub_catalog = ChainedForeignKey(
        SubCatalog,
        verbose_name='Под-каталог',
        chained_field='catalog',
        chained_model_field='catalog',
        show_all=False,
        auto_choose=True,
        sort=True
    )

    name = models.CharField('Имя товара', max_length=255)
    price = models.IntegerField('Цена товара')
    is_currency = models.ForeignKey(Currency, models.SET_NULL, null=True)
    article = models.CharField(max_length=24, unique=True, blank=True, null=True)
    image = models.ImageField(upload_to='./media')
    @property
    def imageURL(self):
        try:
            return self.image.url
        except:
            return ''

    def clean(self):
        if self.name == 'Тамеки' or self.name == 'тамеки':
            raise ValidationError("Мы не продаем табачные изделья")

    def __str__(self):
        return self.name


class Costumer(models.Model):
    class Meta:
        verbose_name = 'Покупатель'
        verbose_name_plural = 'Покупатели'

    G = (
        ('m', 'Мужчина'),
        ('f', 'Женщина')
    )

    name = models.CharField('Имя', max_length=255)
    phone_number = models.TextField('Телефон номер')
    age = models.PositiveIntegerField('Возраст')
    mail = models.EmailField('Почта')
    gender = models.CharField(max_length=10, choices=G)
    location = models.CharField('Адресс доставки:', max_length=255)

    def clean(self):
        s = self.phone_number
        check = ['20', '22', '31', '32', '34', '36', '39', '50', '51',
                 '52', '54', '55', '56', '57', '70', '75', '77', '99']

        if len(s) < 9 or len(s) > 13:
            raise ValidationError('Non Kyrgyzstan number!!!')
        else:
            if len(s) == 9:
                if s[:2] not in check:
                    raise ValidationError('Non Kyrgyzstan number!!!')
            elif len(s) == 10:
                if s[0] != '0' or s[1:3] not in check:
                    raise ValidationError('Non Kyrgyzstan number!!!')
            elif len(s) == 12:
                if s[:3] != '996' or s[3:5] not in check:
                    raise ValidationError('Non Kyrgyzstan number!!!')
            elif len(s) == 13:
                if s[:4] != '+996' or s[4:6] not in check:
                    raise ValidationError('Non Kyrgyzstan number!!!')
            else:
                raise ValidationError('Non KG number!')

    def __str__(self):
        return f'{self.name}'


class Order(models.Model):
    class Meta:
        verbose_name = 'Заказ'
        verbose_name_plural = 'Заказы'

    costumer_name = models.ForeignKey(verbose_name='Имя покупателья', to=Costumer, on_delete=models.SET_NULL, null=True)
    PAY_TYPE = {
        ('cash', 'Наличными'),
        ('card', 'Карта'),
        ('e-card', 'Электронный кошалалёк')
    }
    pay_type = models.CharField(verbose_name='Способ оплаты?', max_length=40, choices=PAY_TYPE, null=True)
    catalog = models.ForeignKey(verbose_name='Каталог', to=Catalog, on_delete=models.SET_NULL, null=True)

    sub_catalog = ChainedForeignKey(
        SubCatalog,
        verbose_name='Под-каталог',
        chained_field='catalog',
        chained_model_field='catalog',
        show_all=False,
        auto_choose=True,
        sort=True
    )
    quality = models.IntegerField('Количество')
    total_price = models.IntegerField('Общая сумма')

    def __str__(self):
        return f'{self.costumer_name}'


class ItemOrder(models.Model):
    class Meta:
        verbose_name = 'Заказ'
        verbose_name_plural = 'Заказы'

    order_article = models.CharField(verbose_name='Артикул заказа ', max_length=255)
    product = models.ForeignKey(verbose_name='Продукт', to=Product, on_delete=models.SET_NULL, null=True)
    order = models.ForeignKey(to=Order, on_delete=models.SET_NULL, null=True)
    quality = models.IntegerField()


class Comment(models.Model):
    class Meta:
        verbose_name = 'Комментарий'
        verbose_name_plural = 'Комментарии'

    text = models.TextField(verbose_name='Текст')
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    owner = models.ForeignKey(User, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)


