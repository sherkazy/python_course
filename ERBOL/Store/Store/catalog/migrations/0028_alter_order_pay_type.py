# Generated by Django 3.2 on 2022-06-23 10:32

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0027_alter_order_pay_type'),
    ]

    operations = [
        migrations.AlterField(
            model_name='order',
            name='pay_type',
            field=models.CharField(choices=[('cash', 'Наличными'), ('e-card', 'Электронный кошалалёк'), ('card', 'Карта')], max_length=40, null=True, verbose_name='Способ оплаты?'),
        ),
    ]
