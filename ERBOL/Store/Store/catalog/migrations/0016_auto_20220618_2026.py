# Generated by Django 3.2 on 2022-06-18 14:26

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0015_auto_20220618_2025'),
    ]

    operations = [
        migrations.AlterField(
            model_name='itemorder',
            name='quality',
            field=models.IntegerField(),
        ),
        migrations.AlterField(
            model_name='order',
            name='pay_type',
            field=models.CharField(choices=[('e-card', 'Электронный кошалалёк'), ('cash', 'Наличными'), ('card', 'Карта')], max_length=40, null=True, verbose_name='Способ оплаты?'),
        ),
    ]
