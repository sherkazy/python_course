# Generated by Django 3.2 on 2022-06-18 14:25

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0014_auto_20220618_2017'),
    ]

    operations = [
        migrations.AddField(
            model_name='order',
            name='total_price',
            field=models.ImageField(default=1, upload_to='', verbose_name='Общая сумма'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='order',
            name='pay_type',
            field=models.CharField(choices=[('cash', 'Наличными'), ('card', 'Карта'), ('e-card', 'Электронный кошалалёк')], max_length=40, null=True, verbose_name='Способ оплаты?'),
        ),
        migrations.CreateModel(
            name='ItemOrder',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('quality', models.ImageField(upload_to='')),
                ('order', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='catalog.order')),
                ('product', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='catalog.product', verbose_name='Продукт')),
            ],
            options={
                'verbose_name': 'Заказ',
                'verbose_name_plural': 'Заказы',
            },
        ),
    ]
