import json
from catalog.models import *
from django.http import HttpResponse
from django.shortcuts import render, redirect


def header(request):
    return render(request, 'header.html')


def catalog(request):
    cat = Catalog.objects.all
    return render(request, 'catalog.html', context={'catalog': cat})


def sub_catalog(request):
    sub = SubCatalog.objects.all
    return render(request, './catalog/sub_catalog.html', context={'sub_catalog': sub})


def products(request):
    product = Product.objects.all
    return render(request, 'product.html', context={'product': product})


def c_subs(request, catalog_id):
    c_sub = SubCatalog.objects.filter(catalog_id=catalog_id)
    return render(request, 'sub_catalog.html', context={'sub_catalog': c_sub})


def s_products(request, catalog_id, sub_id):
    s_product = Product.objects.filter(sub_catalog_id=sub_id)
    return render(request, 'product.html', context={'product': s_product})


def sub_cat(request):
    sub = SubCatalog.objects.all()
    return render(request, 'catalog/sub_catalog.html', context={'sub': sub_cat})


def only_s_products(request, s_id):
    pro = Product.objects.filter(sub_catalog__id=s_id)
    return render(request, 'product.html', context={'product': pro})


def add_comment(request, id):
    product = Product.objects.get(id=id)
    text = request.POST['text']
    comment = Comment(
        text=text,
        product=product,
        owner=request.user
    )
    comment.save()
    return redirect('product', id=comment.id)
#
# def client(request):
#     fname = request.POST['fname']
#     lname = request.POST['lname']
#     age = request.POST['age']
#     gender = request.POST['gender']
#     model = Costumer()
#
#     model.name = fname
#     model.lname = lname
#     model.age = age
#     model.gender = gender
#
#     model.save()
#
#     return redirect('client')
