from django.urls import path
from . import views

urlpatterns = [
    path('', views.catalog, name='catalog'),
    path('sub-catalogs', views.sub_catalog, name='sub_catalog'),
    path('sub-catalog/products', views.products, name='product'),
    path('<int:catalog_id>/sub-catalogs', views.c_subs, name='catalogs_subs'),
    path('<int:s_id>/products', views.only_s_products, name='only_s_products'),
    path('<int:catalog_id>/<int:sub_id>/products', views.s_products, name="subs_product"),
    path('add_comment/<int:id>', views.add_comment, name='add_comment')
    # path('client', views.client, name='client')
]