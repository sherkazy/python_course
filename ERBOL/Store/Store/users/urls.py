from django.urls import path, include

from users import views


urlpatterns = [
    path('register', views.register, name='register'),
    path('login_own', views.login_own, name='login_own'),
    path('logout_own', views.log_out, name='logout_own'),
    path('profile', views.profile, name='profile'),
    path('', include('django.contrib.auth.urls'))
    ]