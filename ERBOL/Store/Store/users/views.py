from django.contrib.auth import authenticate, login
from django.contrib.auth.models import User
from django.shortcuts import render, redirect
from django.contrib import messages


def register(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        password2 = request.POST['password2']
        if password2 != password2:
            messages.error(request, 'Password no same')
            return redirect('register')
        user = User()
        user.username = username
        user.set_password(password)
        user.save()
    return render(request, 'users/register.html')


def login_own(request):
    if request.user.is_authenticated:
        return redirect('catalog')

    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']

        user = authenticate(username=username, password=password)

        if user is not None:
            login(request, user)
            messages.success(request, 'Вход выполнен')
            return redirect('catalog')
        else:
            messages.success(request, 'Неправильные данные')
            return redirect('login_own')

    return render(request, 'users/login.html')


def log_out(request):
    return redirect('logout')


def profile(request):
    return render(request, 'users/profile.html')