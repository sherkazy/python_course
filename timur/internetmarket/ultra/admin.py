from django.contrib import admin
from ultra.models import *


class ItemOrderAdmin(admin.TabularInline):
    model = ItemTovar
    extra = 0


class OrderAdmin(admin.ModelAdmin):
    inlines = [ItemOrderAdmin]


class ClientAdmin(admin.ModelAdmin):
    list_display = ['name', 'login']


class SubKatalogAdmin(admin.ModelAdmin):
    list_display = ['title', 'catalog']
    list_filter = ('catalog',)


class TovarAdmin(admin.ModelAdmin):
    list_display = ['name', 'price']
    list_filter = ('price',)


class BasketAdmin(admin.ModelAdmin):
    exclude = ['total_item', 'total_price']

admin.site.register(Katalog)
admin.site.register(SubKatalog, SubKatalogAdmin)
admin.site.register(Tovar, TovarAdmin)
admin.site.register(Client, ClientAdmin)
admin.site.register(Basket, OrderAdmin)
